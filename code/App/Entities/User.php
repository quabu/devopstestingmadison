<?php
    
    namespace App\Entities;
    
    class User {
        
        private $username;
        private $orgname;
        private $scope;
        private $token;
        private $idOperation;
        private $project;
        private $resource;
        private $internalNameOrg;
        private $maxRequest;
        
        
        /**
         * Returns the value of a property using
         * the dynamic variable names declaration
         * 
         * @param string $propertyName
         *
         * @return string,array
         */        
        public function getProperty($propertyName){
            return $this->{$propertyName};
        }
        
        /**
         * Set the value of a property using
         * the dynamic variable names declaration
         *
         * @param string $propertyName
         * @param string $value
         */
        public function setProperty($propertyName, $value){
            $this->{$propertyName} = $value;
        }
        
    }
    