<?php

    return [
        'db'    => [

        ],
        'auditor' => [

        ],
        'logger' => [

        ],
        'general' => [
            'resources' => [
                '/principal'     => 'SMS_PPAL',
                '/cancellation'  => 'SMS_CANCEL',
                '/acceptation'   => 'SMS_CONFIRM',
                '/others'        => 'SMS_OTROS',
                '/reminder'      => 'SMS_REMINDER',
                '/help'          => 'SMS_HELP',
                '/expiration'    => 'EXPIRE',
            ],
        ],
    ];