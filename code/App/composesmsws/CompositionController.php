<?php
/**
 * @author Luis María Pérez <luismaria.perez@madisonmk.com>
 * @version 1.1
 */
namespace App\composesmsws;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \App\composesmsws\CompositionModel as Model;

class CompositionController
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @var Auditor
     */
    private $Auditor;

    /**
     * @var array
     */
    private $settings;

    /**
     * Creates the controller
     *
     * @param CompositionModel $wsMdl WS model
     * @param array            $settings
     *
     * @return void
     */
    public function __construct(Model $model, array $settings)
    {
        $this->model    = $model;
        $this->settings = $settings;

    }

    /**
     * Get the body and the SMS type
     * and makes up the text
     *
     * @param Request  $request  Slim request object
     * @param Response $response Slim response object
     * @param array    $args
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, array $args)
    {
        $type   = $request->getUri()->getPath();
        $this->model->validateResource($type);

        $params = $request->getParsedBody();

        $output = [
            "status" => "OK",
            "text_sms" => $this->model->compose($params, $type )
        ];

        return $response->withJson($output);
    }
}