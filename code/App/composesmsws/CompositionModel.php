<?php
/**
 * @author Luis María Pérez <luismaria.perez@madisonmk.com>
 * @version 1.1
 */
namespace App\composesmsws;

use \Illuminate\Database\Capsule\Manager as Database;
use \Exception;
use \Twig_Environment as Twig;

class  CompositionModel
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var array
     */
    private $settings;

    /**
     * Creates the model
     *
     * @param Database $dbHandler Eloquent Capsule Manager
     * @oaran array    $settings
     *
     * @return void
     */
    public function __construct(Twig $twig, array $settings )
    {
        $this->twig       = $twig;
        $this->settings   = $settings;
    }

    /**
     *  Makes up the SMS by replacing the text 'anchors'
     *  that match the indexes of the parameters provided.
     *  If the text is not provided, it will get
     *  the default text of the BBDD according to the
     *  required message type.
     *
     *  @param array $data key value list for anchors replacement
     *  @param array $type sms type to make up
     *
     *  @return string
     */
    public function compose(array $data, string $type): string
    {
        $type = $this->settings['resources'][$type];

        if(empty($data['text_sms']))
            throw new Exception('sms text needed', 401);
        
        $textSms = $data['text_sms'];
        unset( $data['text_sms'] );

        $template = $this->twig->createTemplate($textSms);
        return $template->render($data['vars']);
    }

    /**
     * Validate the resource name
     *
     * @param string $resource
     *
     * @return void
     *
     * @throws Exception when the resource doesn't exist
     */
    public function validateResource(string $resource)
    {
        $avialables = array_keys($this->settings['resources']);

        if(!in_array($resource,$avialables))
            throw new Exception("Operation $resource not found");
    }
}