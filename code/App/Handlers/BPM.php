<?php

    namespace App\Handlers;
    
    use \App\Handlers\HttpRequest;
    
    class BPM {
        
        private $HttpRequest;
        
        public function __construct(HttpRequest $HttpRequest)
        {
            $this->HttpRequest = $HttpRequest;
        }
        
        
        /**
         *  Function composeXML
         *
         *  Compone un XML para enviar al BPM.
         *
         *  @param  array  $data       Estructura de datos con los campos y valores a enviar.
         *  @param  string $metodo     Método del web service al que se está llamando.
         *  @param  array  $conf       Configuraciones relativas al método
         *
         *  @return array  petición.
         */ 
        public function composeRequest( $data, $conf){
            
            $query = $conf['queryString'];
            
            $query['params'] = json_encode($data);
            
            $request['query'] = http_build_query($query);
            
            $request['body'] = '';
            
            $request['uri'] = $conf['schema'] . '://' . $conf['host'] . $conf['path'] ;
            
            $request['url'] = $request['uri'] . '?' . $request['query'];
            
            $request['headers'] = $conf['headers'];

            return $request;
        }
        
        /**
         *  Function callSoapService
         *
         *  Realiza una conexión con el BMP.
         *
         *  @param string $xml XML con la peticion SOAP al BPM
         *  @param string $url Direccion HTTP del BPM
         *  @param string $headers (opcional) headers de la petición
         *
         *  @return string Respuesta de la petición
         *  
         */
        public function sendRequest($request)
        {
            
            $this->HttpRequest->setHeaders($request['headers']);
            
            return $this->HttpRequest->send($request['url'], $request['body'], true);
        }
        
        /**
         * Function waitResponse
         *
         * Configure the TimeOut for the the connection
         *
         * @param bool $wait If is true, wait until de system throws
         *                   a timeout, else timeout will be 0
         * 
         */
        public function waitResponse($wait)
        {
            if($wait == true)
                $this->HttpRequest->unsetTimeOut();
            if($wait == false)
                $this->HttpRequest->setTimeOut(1);
        }
        
    }