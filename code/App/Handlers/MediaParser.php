<?php
    
    namespace App\Handlers;
    
    use \Exception;
    
    class MediaParser {
        
        private $bodyParsers;
        
        public function __construct(){
            $this->registerMediaTypeParser('application/json', function ($input) {
                return json_decode($input, true);
            });
    
            $this->registerMediaTypeParser('application/xml', function ($input) {
                $backup = libxml_disable_entity_loader(true);
                $result = simplexml_load_string($input);
                libxml_disable_entity_loader($backup);
                return (array)$result;
            });
    
            $this->registerMediaTypeParser('text/xml', function ($input) {
                $backup = libxml_disable_entity_loader(true);
                $result = simplexml_load_string($input);
                libxml_disable_entity_loader($backup);
                return (array) $result;
            });
            
            /**
             * En caso de una respuesta html se asume que se ha producido
             * un error fatal imposible de capturar y se lanza una excepción
             */
            $this->registerMediaTypeParser('text/html', function($input) {
                
            });
    
            $this->registerMediaTypeParser('application/x-www-form-urlencoded', function ($input) {
                parse_str($input, $data);
                return $data;
            });
        }
        
        public function getParsedBody($body, $content_type)
        {
            $content_type = self::getMediaType($content_type);
            if(gettype($content_type) !== string){
                throw new \Exception(json_encode(
                                     [
                                      'descError'              => 'Fatal Error in the requested resource',
                                      'responseBody'           => $body,
                                      'responseContentType'    => $content_type
                                     ])
                                    );
            }
            return $this->bodyParsers[$content_type]($body);
        }
        
       /**
        * Register media type parser.
        *
        * Note: This method is not part of the PSR-7 standard.
        *
        * @param string   $mediaType A HTTP media type (excluding content-type
        *     params).
        * @param callable $callable  A callable that returns parsed contents for
        *     media type.
        */
       private function registerMediaTypeParser($mediaType, callable $callable)
       {
           if ($callable instanceof Closure) {
               $callable = $callable->bindTo($this);
           }
           $this->bodyParsers[(string)$mediaType] = $callable;
       }
   
       /**
        * Get request media type, if known.
        *
        * Note: This method is not part of the PSR-7 standard.
        *
        * @return string|null The request media type, minus content-type params
        */
       public function getMediaType($contentType)
       {
           if ($contentType) {
               $contentTypeParts = preg_split('/\s*[;,]\s*/', $contentType);
   
               return strtolower($contentTypeParts[0]);
           }
   
           return null;
       }
       
        
    }
    