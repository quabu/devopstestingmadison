<?php

    namespace App\Handlers;
    use \App\Entities\User;
    use \App\Handlers\MediaParser;
    use \Exception;
    
    class HttpRequest
    {        
        public $headers;
        private $token;
        private $mediaParser;
        private $UserEntity;
		private $timeOut;
        
        public function __construct()
        {
           $arguments = func_get_args();
           $argsNum   = func_num_args();
           
           if(method_exists($this, $func = '__construct'.$argsNum.'params'))
                call_user_func_array([$this, $func], $arguments);
        }
        
        public function __construct1params(MediaParser $mediaParser)
        {
             $this->mediaParser = $mediaParser;
        }
        
        public function __construct2params(MediaParser $mediaParser, User $user)
        {
             $this->mediaParser = $mediaParser;
             $this->UserEntity  = $user;
        }
        
        /**
         * Add a Header to the request
         * 
         * @param string $name
         * @param string $value
         * 
         */
        public function withHeader($name, $value)
        {
            $this->headers[$name] =  $value;
        }
		
		/**
		 * Set the the headers to send.
		 * Warning! this overwrite all other headers
		 * setted previously
		 *
		 * @param array $header
		 */		
		public function setHeaders(array $headers)
		{
			$this->headers = $headers;
		}
        
        /**
         * Add the Authorization Header to the request
         *
         * @param string $token
         */
        public function withToken($token = "")
        {
            $this->token = (empty($token)) ? $this->UserEntity->getProperty('token') : $token ;
        }
        
        /**
         * Unset the Headers array
         */
        public function cleanHeaders(){
            $this->headers = [];
        }
        
        /**
         * Unset the Token
         */
        public function withoutToken(){
            $this->token = "";
        }
        
        /**
         * Set the timeout for the connection
         *
         * @param int $seconds Seconds until the connection closes
         */
        public function setTimeOut( $seconds )
        {
            $this->timeOut = $seconds;
        }
        
        /**
         * unset the timeout for the connection
         */
        public function unsetTimeOut()
        {
            $this->timeOut = NULL;
        }
        
        /**
         * Send the HTTP Request
         *
         * @param string $url   Request's destination
         * @param array  $body  Request's payload
         *
         * @return array Returned Response
         *
         * @throws Exception The resource is ofline or unreachable
         * @throws Exception The response contains status value 'KO'
         */
        public function send($url, $body, $raw_response = false)
        {
            $handle = curl_init( $url );
            
            curl_setopt($handle, CURLOPT_POST, true);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $body);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            
            $headers = array();
            
            foreach($this->headers as $header => $value){
                $headers[] = $header . ":" . $value;
            }
			
            if(!empty($this->token))
                 $headers[] = "Authorization: Bearer ".$this->token;
            
            if(count($headers)>0)
                curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
            
            if($this->timeOut !== NULL)
                curl_setopt($handle, CURLOPT_TIMEOUT_MS, $this->timeOut);
            
            $result = curl_exec($handle);
            
            $contentType = curl_getinfo($handle, CURLINFO_CONTENT_TYPE);
            
            curl_close($handle);
            
            if(!$result)
                throw new Exception("Internal error: can't connect with the resource requested.");
            
            if($raw_response === true){
                return $result;
            }
            
            $payload = $this->mediaParser->getParsedBody($result, $contentType);
            
            if($payload['status'] == 'KO')
                throw new Exception($payload['descError']);
                       
            
            return $payload;
        }
    }    