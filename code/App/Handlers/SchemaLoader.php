<?php

    namespace App\Handlers;
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
    
    class SchemaLoader {
        private $basepath;
        
        public function __construct($basepath){
            $this->basepath = $basepath;
        }
        
        public function __invoke(Request $request, Response $response, array $args){
            
            $uri  = $request->getUri();
            $path = $uri->getPath();
            
            $path = explode('/', $path);
            array_shift($path);
            $route = implode('/',
                        array_replace($path, [
                            0 => $path[1],
                            1 => $path[0],
                        ])
                    );
            
            if($route[0] != '/'){
            	$route = '/' . $route;
            }
            
            $data = file_get_contents($this->basepath . $route ."/schema.json");
            
            return $response
                    ->withStatus(200)
                    ->withHeader('Content-Type','application/json')
                    ->write($data);
        }
        
    }
    