<?php

    namespace App\Handlers;
    
    class PathPatternResolver {
        /**
         * Get a given pattern and returns
         * the clean original path without the arguments
         *
         * @param string $pattern   ex: /notifinws/add/{id_container}[/withfile]
         * @param string $path      ex: /notifinws/add/1234/withfile
         *
         * @param string Clean path ex: /notifinws/add/withfile
         */
        
        public function resolve($pattern, $path){
			
			$path    = explode("/", $path);
			
			$rPatterns = ['/\/{[a-zA-Z0-9]+}/', '/\[/' , '/\]/'];
			$replacement = [''];
			$pattern = preg_replace($rPatterns, $replacement, $pattern);
			
			$pattern = explode('/', $pattern);
			
			$resolvedPath = array_intersect($pattern, $path);		
			
			return implode('/', $resolvedPath);
		}
        
    }
    