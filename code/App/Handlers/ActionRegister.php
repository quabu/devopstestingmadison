<?php
/**
 * @author Luis María Pérez <luismaria.perez@madisonmk.com>3
 * @version 1.0
 * 
 */
    namespace App\Handlers;
    
	use Illuminate\Database\Capsule\Manager as Capsule;
    use App\Entities\User;
	use Exception;
    
	/**
	 * This class provide a common inteface
	 * to the for make the insert in the
	 * audit table.
	 *
	 * It uses a pre-populated User Entity
	 * to get token data.
	 *
	 * @uses \Iluminate\Database\Capsule\Manager
	 * @uses \App\Entities\User
	 */
    class ActionRegister {
		
        /**
		 * @var User
		 */
        private $user;
		
		/**
		 * @var Capsule
		 */
		private $dbHandler;
		
		/**
		 * Destination table
		 *
		 * @var string
		 */
		private $dbTable;
		
		/**
		 * WS method requested
		 *
		 * @var string
		 */
		private $method;
		
		/**
		 * Current operation ID. The same for all the WS
		 * called in with the request.
		 * Given by the token
		 *
		 * @var string
		 */
		private $idOperation;
		
		/**
		 * Generated in the construction
		 *
		 * @var int
		 */
		private $idAudit;
		
		/**
		 *
		 * @var string.
		 */
		private $ip;
        
		/**
		 * Creates a new ActionRegister
		 *
		 * @param Capsule 	$dbHandler 	Eloquent Capsule Manager
		 * @param User		$user		Pre-populated User Entity
		 */
        public function __construct(Capsule $dbHandler, User $user){
            $this->dbHandler   = $dbHandler;
            $this->user        = $user->getProperty('username');
            $this->idOperation = $user->getProperty('idOperation');
			
			self::setIdAudit();
        }
        
		/**
		 * Creates a new row in the Audit Table of the WS.
		 *
		 * @param array		$data key => value array corresponding with
		 * 						  the fields in the DB
		 * @param string	[$table]
		 * @param bool		[$override] if true: no additional info is
		 * 								added to $data
		 *
		 * @throws Exception if something goes wrong in the ORM query
		 */
        public function insert( $data = [], $table = "", $override=false ){
            
            $table = (empty($table)) ? $this->dbTable : $table;
			
			if(empty($override)){
				$data['user']   = $this->user;
				$data['method'] = (empty($this->method)) ? $data['method'] : $this->method;
				$data['id_operation'] = (string) $this->idOperation;
			}
			
			try{				
				$audit = $this->dbHandler->table($table);				
				$audit->insert($data);

			}catch(Exception $e){
				throw new Exception($e->getMessage());
			}
        }
		
		/**
		 * Updates a row in the Audit Table of the WS.
		 *
		 * @param array		$data key => value array corresponding with
		 * 						  the fields in the DB
		 * @param array		$conditions key => value Constrains of the query
		 * @param string	[$table]
		 *
		 * @throws Exception if something goes wrong in the ORM query
		 */        
        public function update($data, $conditions, $table = "" ){
            
        	$table = (empty($table)) ? $this->dbTable : $table;
        	
        	try{				
				$audit = $dbHandler->table($table);				
				$audit
					->where($conditions)
					->update($data);
						
			}catch(Exception $e){
				throw new Exception($e->getMessage());
			}
        }
        
		/**
		 * Configures basic data of the Action Register
		 *
		 * @param string $user 		Client who request
		 * @param string $method	WS requested resource
		 * @parma string $dbTable	DB table to write
		 */
        public function config($user, $method, $dbTable){
            $this->user     = $user;
            $this->method   = $method;
            $this->dbTable  = $dbTable;
        }
        
		/**
		 * @param string $user
		 */
        public function setUser( $user ){
            $this->user = $user;
        }
        
		/**
		 * return string
		 */
        public function getUser(){
            return $this->user;
        }
        
		/**
		 * @param string $method
		 */
        public function setMethod( $method ){
            $this->method = $method;
        }
        
		/**
		 * return string
		 */
        public function getMethod(){
            return $this->method;
        }
        
		/**
		 * @param $string $table
		 */
        public function setTable( $table ){
            $this->dbTable = $table;
        }
		
		/**
		 * Set an Unique hexadecimal value
		 */
		public function setIdAudit()
		{
			$this->idAudit = hexdec(uniqid());
		}
		
		/**
		 * @return string
		 */
		public function getIdAudit()
		{
			return $this->idAudit;
		}
		
		/**
		 * @param string $ip
		 */
		public function setIp($ip)
		{
			$this->ip = $ip;
		}
		
		/**
		 * @return string
		 */
		public function getIp()
		{
			return $this->ip;
		}      
    }
    