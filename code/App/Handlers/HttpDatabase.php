<?php

    namespace App\Handlers;
    
    use App\Handlers\HttpRequest;
    use App\Entities\User;
    
    class HttpDatabase {
        
        private $HttpRequest;
        private $settings;
        private $project;
        
        public function __construct(HttpRequest $HttpRequest, User $user, array $settings)
        {
           $this->HttpRequest   = $HttpRequest;
           $this->settings      = $settings;
           $this->project       = $user->getProperty('project');
        }
        
        public function select($table, $fields, $conditions, $arrType, $subQuery = "")
        {
            $url = [
                    $this->settings['urlConnectdbWS'],
                    $this->project,
                    'select'
            ];
            
            $payload = [
                "table"      => $table,
                "fields"     => $fields,
                "conditions" => $conditions,
                "fieldTypes" => $arrType,
                "subQuery"   => $subQuery
            ];
            
            $url = implode('/', $url);
            $payload = json_encode($payload);
            
            $returned = $this->HttpRequest->send($url, $payload);

            return $returned['result'];       
        }
        
        public function insert($table, $fields, $conditions, $arrType, $subQuery = "")
        {
            $url = [
                    $this->settings['urlConnectdbWS'],
                    $this->project,
                    'insert'
            ];                   
            
            $payload = [
                "table"      => $table,
                "fields"     => $fields,
                "conditions" => $conditions,
                "fieldTypes" => $arrType,
            ];
            
            $url = implode('/', $url);            
            $payload = json_encode($payload);
            
            $returned = $this->HttpRequest->send($url, $payload);
            
            if(isset($returned['result']))
                return $returned['result'];
        }
        
        public function update($table, $fields, $conditions, $arrType, $subQuery = "")
        {
            $url = [
                    $this->settings['urlConnectdbWS'],
                    $this->project,
                    'update'
            ];
            
            $payload = [
                "table"      => $table,
                "fields"     => $fields,
                "conditions" => $conditions,
                "fieldTypes" => $arrType,
                "subQuery"   => $subQuery
            ];
            
            $url = implode('/', $url);            
            $payload = json_encode($payload);
            
            $returned = $this->HttpRequest->send($url, $payload);
            
            if(isset($returned['result']))
                return $returned['result']; 
        }
        
        public function multiInsert(array $multipleQueries)
        {            
             $url = [
                $this->settings['urlConnectdbWS'],
                $this->project,
                'multiinsert'
            ];
            
            $url = implode('/', $url);
            $payload = json_encode($multipleQueries, JSON_PRETTY_PRINT);
            
            $returned = $this->HttpRequest->send($url, $payload);
            
            if(isset($returned['result']))
                return $returned['result']; 
        }
        
        public function raw($query) // <-- NOT YET IMPLEMENTED
        {
            $url = [
                    $this->settings['urlConnectdbWS'],
                    $this->project,
                    'raw'
                ];
            
            $payload = [
                "query"   => $query
            ];
            
            $url = implode('/', $url);            
            $payload = json_encode($payload);
            
            $returned = $this->HttpRequest->send($url, $payload);
            
            if(isset($returned['result']))
                return $returned['result'];
        }
    }
    