<?php

namespace App\Handlers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


class NotAllowed
{

    public function __invoke(Request $request, Response $response, array $methods)
    {        
        return $response
                ->withStatus(405)
                ->withHeader('Allow', implode(', ', $methods))
                ->withHeader('Content-type', 'application/json')
                ->withJson([
                            "descError" => "The method should be one of the following: ".implode(', ', $methods),
                            "status"    => "KO"
                            ]);
    }
}
