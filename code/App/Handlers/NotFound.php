<?php

namespace App\Handlers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


class NotFound
{

    public function __invoke(Request $request, Response $response)
    {        
        return $response
                ->withStatus(404)
                ->withHeader('Content-type', 'application/json')
                ->withJson([
                            "descError" => "Operation " . $request->getUri()->getPath() . " not found",
                            "status"    => "KO"
                            ]);
    }
}