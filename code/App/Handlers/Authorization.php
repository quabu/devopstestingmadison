<?php

    namespace App\Handlers;
    
    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \App\Handlers\Database;
    use \App\Handlers\Unauthorized;
    use \App\Handlers\PathPatternResolver as PathResolver;
    use \App\Entities\User;
    use \Exception;
    
    class Authorization
    {
        private $user;
        private $PathResolver;
        private $settings;
        
        public function __construct(User $user, PathResolver $pathResolver)
        {
            $this->user = $user;
            $this->PathResolver = $pathResolver;
        }
        
        /**
         * Check if a user can Access to the
         * requested resource
         *
         * @param Request $request PSR7 request object
         * @param Response $response PSR7 response object
         * @param array $arguments Decoded payload of JWT
         *
         * @throws Exception 401 Unauthorized
         */
        public function canAccess(Request $request, $decoded)
        {
            $resource = $request->getUri()->getPath();
		
			$attributes = $request->getAttributes();
			$pattern = $attributes ['route']->getPattern();
			$module = $request->getUri ()->getHost();
			
			$resource = explode ( '/', $this->PathResolver->resolve ( $pattern, $resource ) );
			$module = explode ( '-', $module );
			
			if (count ( $module ) > 1)
				$module = $module [1];
			else
				$module = 'unknown';
			if (count ( $resource ) <= 1)
				$resource = '/' . $module . 'ws/' . $module . 'ws';
			else
				$resource = '/' . $module . 'ws' . implode ( '/', $resource );
			
			if ( empty($decoded['scope']) || !in_array( $resource, $decoded ['scope'] ))
				throw new \Exception ( "Unauthorized Access to the resource: $resource", 401 );
			
			$this->user->setProperty( 'username', $decoded ['username'] );
			$this->user->setProperty( 'orgname', $decoded ['orgname'] );
			$this->user->setProperty( 'scope', $decoded ['scope'] );
			$this->user->setProperty( 'project', $decoded ['project'] );
			$this->user->setProperty( 'idOperation', $decoded ['idOperation'] );
			$this->user->setProperty( 'internalNameOrg', $decoded['internalNameOrg']);
			$this->user->setProperty( 'maxRequest', $decoded['maxRequest']);
			
			$authHeader = $request->getHeader( 'Authorization' );
			
			$jwtEncoded = trim( str_replace ( 'Bearer', '', array_shift ( $authHeader ) ) );
			
			$this->user->setProperty( 'token', $jwtEncoded );
		
        }
        
    }
    

?>