<?php

namespace App\Handlers;

use Psr\Http\Message\ ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


final class Error extends \Slim\Handlers\Error
{

    public function __invoke(Request $request, Response $response, \Exception $exception)
    {
        
        $body = json_decode($exception->getMessage(), true);
        
        if($body == ""){
            $body['descError'] = $exception->getMessage();
        }
        
        $body['status'] = 'KO';
        $status = ($exception->getCode() == 0) ? 500 : $exception->getCode();
        $body['cod_error'] = $status;
        
        return $response
                ->withStatus($status)
                ->withHeader('Content-type', 'application/json')
                ->withJson($body);
    }
}

?>