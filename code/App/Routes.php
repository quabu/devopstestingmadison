<?php
/**
 * YOUR INCREDIBLE AWESOME WEBSERVICE HERE
 */
$app->group('', function(){
	$this->post('/{params:.*}', 'ComposeSmsController');
})->add('SchemaValidationMw');

/**
 * HANDLER SCHEMAS
 */
$app->get('/schemas/{params:.*}', 'SchemaLoaderHandler');

/**
 * GLOBAL MIDDLEWARE
 */
$app->add('CORSMw');
$app->add('RKAIpAddressMw');