<?php
/**
 * == ENTITIES ==
 */
$container['UserEntity'] = function($c) {
    return new \App\Entities\User();
};

/**
 * == HANDLERS ==
 */
$container['errorHandler'] = function ($c) {
    return new \App\Handlers\Error();
};

$container['notFoundHandler'] = function ($c) {
    return new \App\Handlers\NotFound();
};

$container['notAllowedHandler'] = function ($c) {
    return new \App\Handlers\NotAllowed();
};

$container['BPM'] = function ($c) {
    return new \App\Handlers\BPM($c['HttpRequestHandler']);
};

$container['authClass'] = function ($c){
    return new \App\Handlers\Authorization( $c['UserEntity'], $c['PathResolver'] );
};

$container['refResolver'] = function($c) {
    return new JsonSchema\RefResolver(new JsonSchema\Uri\UriRetriever(), new JsonSchema\Uri\UriResolver());
};

$container['jsonSchemaValidator'] = function($c) {
    return new JsonSchema\Validator();
};

$container['SchemaLoaderHandler'] = function ($c) {
    return new \App\Handlers\SchemaLoader( __DIR__ );
};

$container['JwtHandler'] = function($c) {
    return new \Firebase\JWT\JWT();
};

$container['PathResolver'] = function($c) {
    return new \App\Handlers\PathPatternResolver();
};

$container['MediaParserHandler'] = function($c){
    return new \App\Handlers\MediaParser();
};
$container['HttpRequestHandler'] = function($c){
    return new \App\Handlers\HttpRequest($c['MediaParserHandler']);
};

$container['HttpInternalsHandler'] = function($c){
    $HttpRequest = new \App\Handlers\HttpRequest($c['MediaParserHandler'], $c['UserEntity']);
    $HttpRequest->withHeader('Content-Type','application/json');

    return $HttpRequest;
};

/**
 * == MIDDLEWARE ==
 */

$container['CORSMw'] = function($c) {
    return new \App\Middleware\CORS();
};

$container["RKAIpAddressMw"] = function($c) {
    return new \RKA\Middleware\IpAddress();
};

$container['validationSettings'] = [
    'urlSchemasHandler' => $container['request']->getUri()->getBaseUrl(),
];

$container['SchemaValidationMw'] = function($c) {
    return new \App\Middleware\Validation($c['refResolver'],
                                          $c['jsonSchemaValidator'],
                                          $c['PathResolver'],
                                          $c['validationSettings']);
};