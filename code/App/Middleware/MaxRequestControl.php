<?php

namespace App\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \App\Handlers\Database;
use \App\Entities\User;

class MaxRequestControl
{
	private $UserEntity;
	private $dbHanlder;
	private $auditTb;

	public function __construct( Database $database, User $user, $auditTb )
	{
		$this->UserEntity = $user;
		$this->dbHandler  = $database;
		$this->auditTb    = $auditTb;
	}

	public function __invoke(Request $request, Response $response, Callable $next)
	{
		$maxRequest = $this->UserEntity->getProperty('maxRequest');

		if( $maxRequest == 0 || ! empty($request->getParsedBodyParam('apikey')))
		   $next($request,$response);

		$requestCount = self::getRequestCount();

		if($maxRequest <= $requestCount)
			throw new Exception("The request limit of the demo has been reached. Upgrade Required", 426);
		else
			$next($request,$response);
	}

	private function getRequestCount()
	{
		$user = $this->UserEntity->getProperty('username');

		$table = $this->auditTb;
		$fields = ['COUNT(1)'];
		$conditions = ['user' => $user];
		$arrTypes = ['default' => 's'];

		$rs = $this->dbHandler->select($table, $fields, $conditions, $arrTypes);

		return array_shift($rs[0]);
	}
}