<?php

namespace App\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CORS {

    public function __invoke(Request $request, Response $response, $next) {
        $newResponse = $response
                        ->withHeader('Access-Control-Allow-Origin', "*")
                        ->withHeader('Access-Control-Allow-Headers', array(
                            'origin',
                            'content-type',
                            'user-agent',
                            'X-Requested-With',
                            'Accept'
                        ))
                        ->withHeader('Access-Control-Allow-Methods', array('POST', 'GET'))
                        ->withHeader('Content-Type', 'application/json');

        if($request->isOptions()) {
                return $newResponse;
        }

        return $next($request, $newResponse);
    }

}
