<?php
    
    namespace App\Middleware;

    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;
	use \App\Handlers\PathPatternResolver as PathResolver;
    use \JsonSchema\RefResolver;
    use \JsonSchema\Validator;
    use \Exception;
    
    final class Validation {
        
        private $refResolver;
        private $validator;
		private $pathResolver;
        private $settings;
        
        public function __construct(
							RefResolver $refResolver,
							Validator $validator,
							PathResolver $pathResolver,
							array $settings
							){
            $this->refResolver  = $refResolver;
            $this->validator    = $validator;
			$this->pathResolver = $pathResolver;
            $this->settings     = $settings;
		
        }
        
        public function __invoke(Request $request, Response $response, Callable $next){
            
		$data = json_decode ( $request->getBody () );
		
		$uri = $request->getUri ();
		$path = $uri->getPath ();
		
		$attributes = $request->getAttributes ();
		$pattern = $attributes ['route']->getPattern ();
		$module = $request->getUri ()->getHost ();
		
		$module = explode ( '-', $module );
		
		if (count ( $module ) > 1)
			$module = $module [1];
		else
			$module = 'unknown';
		
		$path = '/schemas/' . $module . 'ws' . $this->pathResolver->resolve ( $pattern, $path );
		
		$url = $this->settings ['urlSchemasHandler'];
		
		try {
			$schema = $this->refResolver->resolve ( $url . $path );
		} catch ( Exception $e ) {
			throw new Exception ( $e->getMessage (), 400 );
		}
		
		self::jsonSchema ( $data, $schema );
		
		return $next ( $request, $response );
		
        }
        
        public function jsonSchema($data, $schema){
			
			$this->validator->check($data, $schema);            
		
			if (!$this->validator->isValid()) {
				
			   foreach($this->validator->getErrors() as $error){
					$descError[] = $error['property'] . " " . $error['message'];
			   }              
			   
			   throw new Exception(json_encode(["descError" => $descError]), 400);
			}
        }
		
        
    }
    