<?php
/**
 * WS DEPENDENCIES MUST BE INYECTED HERE
 *
 * DEPENDENCY CONTAINER: $container
 */
$container['ComposeSmsSettings'] = require realpath(__DIR__) . '/composesmsws/config/'.$entorno.'/settings.php';

$container['twig'] = function($c){
	$loader = new \Twig_Loader_Array(array());

	return new \Twig_Environment($loader);
};

$container['ComposeSmsModel'] = function($c){
	return new App\composesmsws\CompositionModel(
		$c['twig'],
		$c['ComposeSmsSettings']['general']
	);
};

$container['ComposeSmsController'] = function($c){
	return new App\composesmsws\CompositionController(
		$c['ComposeSmsModel'],
		$c['ComposeSmsSettings']['general']
	);
};