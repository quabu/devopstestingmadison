# Dockerfile install Jenkins with PHP
FROM ubuntu
MAINTAINER Quabu Solutions SL <support@quabu.eu>

# Prepare Jenkins installation
RUN apt-get update
RUN apt-get -y -f install wget curl apt-transport-https nano
RUN wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add - 
RUN echo "deb https://pkg.jenkins.io/debian-stable binary/" | tee -a /etc/apt/sources.list

RUN apt-get update

# Install Jenkins
RUN apt-get -y -f install jenkins

# Docker install
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN echo "deb https://download.docker.com/linux/ubuntu xenial stable/" | tee -a /etc/apt/sources.list
RUN apt-get update
RUN apt-get -y -f install docker.io

# PHP installation
RUN apt-get -y -f install php-cli php-dev php-curl curl php-pear ant

# PHP Unit installation
RUN wget https://phar.phpunit.de/phpunit.phar
RUN chmod +x phpunit.phar
RUN mv phpunit.phar /usr/local/bin/phpunit

# Postman/Newman Installation
RUN apt-get -y -f install npm
RUN npm install -g newman
RUN ln -s /usr/bin/nodejs /usr/bin/node

# Taurus Installation
# Pip
RUN apt-get -y -f install python-pip
RUN pip install --upgrade pip
# Taurus dependencies
RUN pip install lxml
RUN pip install psutil
# Taurus
RUN pip install bzt

# Clean installation

RUN apt-get clean -y

VOLUME /var/run/docker.sock:/var/run/docker.sock

# Start services
RUN service jenkins start
